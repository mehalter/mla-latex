mla-latex
=========

This is a LaTeX style formatter package to help create MLA format documents.

# Prerequisites

biblatex v3.7

This requires the latest version of mla-biblatex since development has resumed.
This version is what is found in TexLive 2017.
I have tested with TexLive 2015 and 2016 and neither biblatex versions that were included in the distributions were up to date.

# Installation

To install the package, simply download the `mla.sty` file and place it in the texmf folder for easy access from any LaTeX file.

For individual use, simply place it in the project folder for your LaTeX files.

# Usage

No other packages are required to use this mla package.
The `mla.sty` file uses the following packages: geometry, babel, csquotes, biblatex, times, fontenc, datetime, setspace, ifthen, and fancyhdr.

All you have to do to use it is add the following command:

```
\usepackage{mla}
```

## Header

To create the standard header, you must include the following code block in your preamble:

```
\first{First Name}
\last{Last Name}
\prof{Professor's Name}
\class{Class Name}
\title{Paper Title}
```

By default the paper will compile and use the current date in the header, but a custom date may be used by including the following command while defining the header information:

```
\date{DAY MONTH YEAR}
```

Once the above is added, you can create the header in the body by using ht ecommand:

```
\makeheader{}
```

## Citations

This packages uses BibLaTeX since it is standard when writing LaTeX documents.
All citations should be kept in a `.bib` file and formatted correctly for use with BibLaTeX.
Once the file is created, you can include it by adding the following command in your preamble:

```
\sources{BibFileName.bib}
```

Once that is included you can site sources with or without a page number with the following commands:

```
\cite{SourceName}
\cite[Page]{SourceName}
```

Once you have cited a source, you can put the works cited page in your document by putting the following command at the end of the body:

```
\printbibliography{}
```
